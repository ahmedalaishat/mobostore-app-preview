# [MoboStore](https://drive.google.com/file/d/1-RxYjza9FyCHAX2Pcqkys5VxrhZszyli/view?usp=sharing)
[MoboStore Apk Link](https://drive.google.com/file/d/1-RxYjza9FyCHAX2Pcqkys5VxrhZszyli/view?usp=sharing)

## Sign in

 ![sign in](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/sign%20in.jpg)


## Sign up 

 ![sign up](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/sign%20up.jpg)


## Sign up error
error in sign up

 ![sign up](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/sign%20in%20error.jpg)


## Sign up succes

 ![sign up](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/sign%20in%20success.jpg)


## Mobiles
show all mobiles

 ![mobiles](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/mobiles.jpg)


## Mobile detail
show mobile detail

 ![mobiles](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/mobile%20detail.jpg)
 ![mobiles](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/mobile%20detail%202.jpg)
 
 
## Brands
brands page

 ![brands](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/brands.jpg)


## Cart
cart page

 ![cart](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/cart.jpg)
 
 
## Add to cart
add mobile to cart

 ![cart](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/add%20to%20cart.jpg)
 
 
## Add to favorite
add mobile to favorite

 ![cart](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/favorite%20add.jpg)
 
 
## Navigation view 

 ![nav](https://gitlab.com/ahmedalaishat/mobostore-app-preview/-/raw/master/nav.jpg)
